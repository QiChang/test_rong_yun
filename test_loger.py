# -*- coding: UTF-8 -*-
import json
import logging
import os
from logging.handlers import TimedRotatingFileHandler
LOG_PATH = os.environ.get("LOG_PATH")
if not LOG_PATH:
    LOG_PATH = './logs'

print(LOG_PATH)
if not os.path.isdir(LOG_PATH):
    os.makedirs(LOG_PATH)

# 日志格式管理
# formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
# print(formatter)
# 日志格式管理
formatter = logging.Formatter('%(asctime)s %(message)s')
# 日志格式管理
# formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
# 主要使用的logger
# logger = logging.getLogger("wulai_logger")

# 主要使用的logger
logger = logging.getLogger("测试")

###############################################################################
# # 一般日志存放地址
wulai_loghd_info = TimedRotatingFileHandler(os.path.join(LOG_PATH, 'tornado.log'), when="midnight", encoding='utf-8')
wulai_loghd_info.setFormatter(formatter)
wulai_loghd_info.setLevel(logging.INFO)


# 错误级别以上的日志单独存放，方便查找问题
wulai_loghd_error = logging.FileHandler(os.path.join(LOG_PATH, 'tornado.error.log'),encoding='utf-8')
wulai_loghd_error.setFormatter(formatter)
wulai_loghd_error.setLevel(logging.ERROR)
###############################################################################

 # handler 管理
# logger.addHandler(wulai_loghd_info)
logger.addHandler(wulai_loghd_info)
logger.addHandler(wulai_loghd_error)


def debug(*msg):
    print(*msg)


def info(msg, *args, **kwargs):
    print("sdadsadsd")
    print(msg)
    logger.info(msg, *args, **kwargs)


def error(msg, *args, **kwargs):
    logger.error(msg, *args, **kwargs)


def warning(msg, *args, **kwargs):
    logger.warning(msg, *args, **kwargs)


def pretty_json(json_msg):
    data = json.dumps(json_msg, indent=4, sort_keys=True, ensure_ascii=False)
    print(data)


if __name__ == "__main__":
    debug("debug")
    info("info")
    error("error")
    warning("warning")
