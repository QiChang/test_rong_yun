import smtplib
import traceback
from email.mime.text import MIMEText
from email.header import Header
from contextlib import contextmanager
import os

EMAIL_RECEIVER = os.getenv('receiver', 'yinqichang@laiye.com')
try:
    import test_loger as logger
except ImportError:
    import logging as logger


def sentLog(subject, content, *, receiver=None, from_msg=None):
    # 第三方 SMTP 服务
    mail_host = "smtp.exmail.qq.com"  # 设置服务器
    mail_user = 'notify@laiye.com'  # 用户名
    mail_pass = 'Laiye@2015'  # 口令

    sender = 'notify@laiye.com'

    # 接收邮件，可设置为你的QQ邮箱或者其他邮箱
    if receiver is None and EMAIL_RECEIVER:
        receiver = EMAIL_RECEIVER

    assert receiver is not None, '无接受者'
    if isinstance(receiver, str):
        receivers = [receiver]
    else:
        receivers = receiver

    # 发送者
    if from_msg is None:
        from_msg = sender

    # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
    message = MIMEText(content, 'plain', 'utf-8')

    message['From'] = Header(from_msg, 'utf-8')   # 发送者
    message['To'] = Header(", ".join(receivers), 'utf-8')    # 接收者

    subject = subject
    message['Subject'] = Header(subject, 'utf-8')

    try:
        smtpObj = smtplib.SMTP_SSL(mail_host)
        smtpObj.login(mail_user, mail_pass)
        smtpObj.sendmail(sender, receivers, message.as_string())
        logger.info("[邮件发送成功]")
    except Exception:
        err = traceback.format_exc()
        logger.warning(err)
        print(err)


@contextmanager
def watching(label):
    try:
        yield
    except Exception:
        err = traceback.format_exc()
        logger.error(err)
        sentLog(label, err)


if __name__ == '__main__':
    sentLog('测试', '消息投递超时了')
