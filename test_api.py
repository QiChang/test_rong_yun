# -*- coding: utf-8 -*-

import requests
import json
from test_loger import logger
import inspect
import time
from alarm import sentLog


DEBUG = False
if DEBUG:
    HOST = "http://127.0.0.1:8001"
else:
    LAIYE_HOST = "mplugin.laiye.com"
    WULAI_HOST = "mplugin.wul.ai"


def get_news():
    #获取一个连接中的内容
    url = "http://open.iciba.com/dsapi/"
    r = requests.get(url)
    content = r.json()['content']
    note = r.json()['note']
    return content, note

def mplugin_wulai_http_callback():
    url = 'http://' + WULAI_HOST + "/callback"
    content = get_news()
    str = content[1]
    print(str)
    body = {
        "msg_id": "12312313qq11111",
        "msg_ts": 1537252907768,
        "user_id": "test123",
        "msg_body": {
            "text": {
                "content": str
            },
            "extra": ""},
        "user_request": [],
        "intent": [],
        "sender_info": {
            "real_name": "陆菲",
            "avatar_url": ""

        }
    }
    response = requests.post(url, data=json.dumps(body))
    # 返回信息
    # #print (response.text)
    # 返回响应头
    # #print (response.status_code)
    print('mplugin_wulai_http_callback Code: {} : {}'.format(response.status_code, str))
    return response.status_code

def mplugin_wulai_https_callback():
    url = 'https://' + WULAI_HOST + "/callback"
    content = get_news()
    str = content[1]
    # #print(str)
    body = {
        "msg_id": "12312313qq11111",
        "msg_ts": 1537252907768,
        "user_id": "test123",
        "msg_body": {
            "text": {
                "content": str
            },
            "extra": ""},
        "user_request": [],
        "intent": [],
        "sender_info": {
            "real_name": "陆菲",
            "avatar_url": ""

        }
    }
    response = requests.post(url, data=json.dumps(body))
    # # 返回信息
    # #print (response.text)
    # # 返回响应头
    # #print (response.status_code)
    print('mplugin_wulai_https_callback Code: {} : {}'.format(response.status_code, str))
    return response.status_code


def mplugin_laiye_http_callback():
    url = 'http://' + LAIYE_HOST + "/callback"
    print(url)
    content = get_news()
    str = content[1]
    #print(str)
    body = {
        "msg_id": "12312313qq11111",
        "msg_ts": 1537252907768,
        "user_id": "test123",
        "msg_body": {
            "text": {
                "content": str
            },
            "extra": ""},
        "user_request": [],
        "intent": [],
        "sender_info": {
            "real_name": "陆菲",
            "avatar_url": ""

        }
    }
    response = requests.post(url, data=json.dumps(body))
    # 返回信息
    # print (response.text)
    # # 返回响应头
    print ('mplugin_laiye_http_callback Code: {0} : {1}'.format(response.status_code, str))
    return response.status_code

def mplugin_laiye_https_callback():
    url = 'https://' + LAIYE_HOST + "/callback"
    content = get_news()
    str = content[1]
    # #print(str)
    body = {
        "msg_id": "12312313qq11111",
        "msg_ts": 1537252907768,
        "user_id": "test123",
        "msg_body": {
            "text": {
                "content": str
            },
            "extra": ""},
        "user_request": [],
        "intent": [],
        "sender_info": {
            "real_name": "陆菲",
            "avatar_url": ""

        }
    }
    response = requests.post(url, data=json.dumps(body))
    # 返回信息
    # #print (response.text)
    # # 返回响应头
    # #print (response.status_code)
    print('mplugin_laiye_https_callback Code: {} : {}'.format(response.status_code, str))
    return response.status_code


def mplugin_wulai_https_rongcloud():
    url =  "https://" + WULAI_HOST + '/rongcloud?timestamp=1408710653491&nonce=14314&signature=45beb7cc7307889a8e711219a47b7cf6a5b000e8'
    body = {
    "userid": "apert60592",
    "status": "1",
    "os": "Android",
    "time": "1437982625625"
    }

    try:
        response = requests.post(url, data=json.dumps(body),timeout=1)
        # 返回信息
        # #print(response.text)
        # # 返回响应头
        # #print(response.status_code)
        print('mplugin_wulai_https_rongcloud: {} : {}'.format(response.status_code, response.text))
    except requests.exceptions.Timeout:
        #print("Timeout occurred")
        sentLog("测试","{}Timeout occurred".format(inspect.stack()[1][3]))

    return response.status_code


def mplugin_wulai_http_rongcloud():
    url =  "http://" + WULAI_HOST + '/rongcloud?timestamp=1408710653491&nonce=14314&signature=45beb7cc7307889a8e711219a47b7cf6a5b000e8'
    body = {
    "userid": "apert60592",
    "status": "1",
    "os": "Android",
    "time": "1437982625625"
    }

    try:
        response = requests.post(url, data=json.dumps(body),timeout=1)
        # 返回信息
        # #print(response.text)
        # # 返回响应头
        # #print(response.status_code)
        print('mplugin_wulai_http_rongcloud: {} : {}'.format(response.status_code, response.text))
    except requests.exceptions.Timeout:
        #print("Timeout occurred")
        sentLog("测试","{}Timeout occurred".format(inspect.stack()[1][3]))

    return response.status_code


def mplugin_laiye_https_rongcloud():
    url =  "https://" + LAIYE_HOST + '/rongcloud?timestamp=1408710653491&nonce=14314&signature=45beb7cc7307889a8e711219a47b7cf6a5b000e8'
    body = {
    "userid": "apert60592",
    "status": "1",
    "os": "Android",
    "time": "1437982625625"
    }

    try:
        response = requests.post(url, data=json.dumps(body),timeout=1)
        # 返回信息
        # #print(response.text)
        # # 返回响应头
        # #print(response.status_code)
        print('mplugin_laiye_https_rongcloud: {} : {}'.format(response.status_code, response.text))
    except requests.exceptions.Timeout:
        #print("Timeout occurred")
        sentLog("测试","{}Timeout occurred".format(inspect.stack()[1][3]))

    return response.status_code

def mplugin_laiye_http_rongcloud():
    url =  "http://" + LAIYE_HOST + '/rongcloud?timestamp=1408710653491&nonce=14314&signature=45beb7cc7307889a8e711219a47b7cf6a5b000e8'
    body = {
    "userid": "apert60592",
    "status": "1",
    "os": "Android",
    "time": "1437982625625"
    }

    try:
        response = requests.post(url, data=json.dumps(body),timeout=1)
        # 返回信息
        # #print(response.text)
        # # 返回响应头
        # #print(response.status_code)
        print('mplugin_laiye_http_rongcloud: {} : {}'.format(response.status_code, response.text))
    except requests.exceptions.Timeout:
        #print("Timeout occurred")
        sentLog("测试","{}Timeout occurred".format(inspect.stack()[1][3]))

    return response.status_code

def mplugin_wulai_http_rongcloud():
    url =  "http://" + LAIYE_HOST + '/rongcloud?timestamp=1408710653491&nonce=14314&signature=45beb7cc7307889a8e711219a47b7cf6a5b000e8'
    body = {
    "userid": "apert60592",
    "status": "1",
    "os": "Android",
    "time": "1437982625625"
    }

    try:
        response = requests.post(url, data=json.dumps(body),timeout=1)
        # 返回信息
        # #print(response.text)
        # # 返回响应头
        # #print(response.status_code)
        print('mplugin_laiye_https_rongcloud: {} : {}'.format(response.status_code, response.text))
    except requests.exceptions.Timeout:
        #print("Timeout occurred")
        sentLog("测试","{}Timeout occurred".format(inspect.stack()[1][3]))

    return response.status_code


if __name__ == "__main__":
    # threads = []
    # for i in range(180):
    #     t = threading.Thread(target=test_web_sdk, args=())  # target=show执行show函数，后面args表示show函数中传入的参数
    #     threads.append(t)
    #     # t.start()
    while True:
        mplugin_laiye_http_callback()
        time.sleep(1)
        mplugin_laiye_https_callback()
        time.sleep(1)
        mplugin_wulai_http_callback()
        time.sleep(1)
        mplugin_wulai_https_callback()
        time.sleep(1)
        mplugin_wulai_http_rongcloud()
        time.sleep(1)
        mplugin_wulai_https_rongcloud()
        time.sleep(1)
        mplugin_laiye_https_rongcloud()
        time.sleep(1)
        mplugin_laiye_http_rongcloud()
