# -*- coding: utf-8 -*-
'''
这是一个用来测试微信自动发送消息的demo
参考了一篇博客
博客地址:http://www.cnblogs.com/botoo/p/8622379.html
恩，主要就是用到了一个微信库--wxpy
安装很简单  pip install wxpy
下面就开始吧
主要就两个函数
1、getNews();用以获取信息
2、sendNews();用以发送信息

我这里发送消息用的是for循环本意是群发，但是！但是！但是！程序发的太快会被微信禁掉，大概40个人左右就会被禁，以后可以试试sleep一下。

另外vscode中自定义python编译器：
Ctrl+shift+p, 选择 python： Select Interpreter
'''


from __future__ import unicode_literals
from wxpy import *
import requests
from threading import Timer

itchat = Bot(console_qr=2,cache_path="botoo.pkl")
def getNews():
    url = "http://open.iciba.com/dsapi/"
    r = requests.get(url)
    content = r.json()['content']
    note = r.json()['note']
    print("___________")
    print(content)
    print(note)
    print("==========")
    return content, note

def sendNews():
    try:
        #这里是备注
        friend = itchat.friends().search(name = u'Dearest')

        content = getNews()
        print(content)
        message1 = str(content[0])
        message2 = str(content[1])
        message3 = "xxx"
        print(friend)

        for index,item in enumerate(friend):


            print("发送给 "+str(item)+" ing,index="+str(index))
            item.send(message1)
            item.send(message2)
            item.send(message3)
        t = Timer(1000,sendNews)
        t.start()
    except:
        errorMessage = "xxx"
        for index,item in enumerate(friend):
            item.send(errorMessage)


if __name__ == "__main__":
    sendNews()